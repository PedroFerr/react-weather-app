import './styles.scss';
import React from 'react';
import ReactDOM from "react-dom";

//... and our Components:
import App from './components/App'

console.warn('Your app should now be ready to go!');

const
    root = document.getElementById('app-container')

;

ReactDOM.render(
    <App /> //Same as <App></App>
    , root
);
