import React, { Component } from 'react';
import WeatherListItem from './WeatherListItem.js';

class WeatherList extends Component{

    render() {
        const
            {days, onDayClicked} = this.props;

        ;
        return(
            <div className="weather-list flex-parent">

                {days.map((day, idx) =>

                    <WeatherListItem
                        //React needs an UNIQUE index so he can compare items:
                        key          = {day.dt}
                        //And each item to show:
                        day          = {day}    //each of dates Array of Objs
                        dayIndex     = {idx}    //the index of each of the dates, in the array
                        //And the events, binded at App.js but occured at this (child) component:
                        onDayClicked = {onDayClicked}
                    />
                
                )}
            
            </div>
        )
    }
}

export default WeatherList;