import React, { Component } from 'react';

class CurrentDay extends Component{
    render(){
        const
            {city, day: { temp, weather, pressure, humidity, speed, deg } } = this.props
            , cityName = city.name
            , icon     = weather[0].icon
            , descr    = weather[0].description
        ;
    
          return (
            
            <div className="current-day">
                <h1 className="day-header">{cityName}</h1>
                <div className="weather">
                    <p>
                        <img src={`http://openweathermap.org/img/w/${icon}.png`} alt={descr}/><span>{descr}</span>
                    </p>
                    </div>
                <div className="details flex-parent">
                    <div className="temperature-breakdown">
                    <p>Morning Temperature: {temp.morn}&deg;F</p>
                    <p>Day Temperature: {temp.day}&deg;F</p>
                    <p>Evening Temperature: {temp.eve}&deg;F</p>
                    <p>Night Temperature: {temp.night}&deg;F</p>
                    </div>
                    <div className="misc-details">
                    <p>Atmospheric Pressure: {pressure} hPa</p>
                    <p>Humidity: {humidity}%</p>
                    <p>Wind Speed: {speed} mph</p>
                    </div>
                </div>
            </div>

          );
    }
}

export default CurrentDay;