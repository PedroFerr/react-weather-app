import React, { Component } from 'react';


class WeatherListItem extends Component{

    constructor(props){
        super(props);

        this.onClick = this.onClick.bind(this);
    }

    //Methods:
    onClick(){
        const
            {onDayClicked, dayIndex} = this.props

        ;
        //Communicate with parent through his JSX (HTML) 'dayIndex var':
        onDayClicked(dayIndex);
        //Pass up the the index of the clicked elem
    }



    render(){
        const
            {day} = this.props
            , date = new Date(day.dt*1000)

        ;

        return(
            <div className="weather-list-item" onClick={this.onClick}>
                <h2>{date.getMonth() + 1} / {date.getDate()}</h2>
                <h3>{day.temp.min.toFixed(1)}&deg;F &#124; {day.temp.max.toFixed(1)}&deg;F</h3>
            </div>
        );
    }
}


export default WeatherListItem;