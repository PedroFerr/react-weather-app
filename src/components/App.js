import React from 'react';
//Library for making AJAX requests:
import {get} from 'axios';


//... and our Components:
import ZipForm from './ZipForm';
import WeatherList from './WeatherList';
import CurrentDate from './CurrentDate';

//Create a component:
class App extends React.Component {

    //Initialize the satte of this component
    constructor(props){
        super(props);

        this.state = {
            weatherData: []
            , zipcode: ''
            , city : {}
            , dates: []
            , selectedDate: null
        }

        this.onFormSubmit = this.onFormSubmit.bind(this);
        //Bind the clicked (index) date:
        this.onDayClicked = this.onDayClicked.bind(this);        

    };

    //Life cycle method (once the App is 1st loading):
    componentDidMount(){
        get(`http://localhost:3000/weather`)
            .then(({ data: weatherData }) => {
                this.setState( {weatherData} );

            })
        ;
    }
    

    //Class Methods:
    onFormSubmit(zipcode){
        /*
        //this.setState({zipcode});
        get(`http://localhost:3000/weather/${zipcode}`)
            .then(({ data }) => {
                const {
                    city,
                    list: dates
                } = data;
            
                this.setState({ zipcode, city, dates, selectedDate: null });
            })
        
        ;
        */
        const
            zip                     = zipcode * 1
            , { weatherData }       = this.state
            , data                  = weatherData.find(wd => wd.id === zip)
            , { city, list: dates } =  data
        ;
      
        this.setState({ zip, city, dates, selectedDate: null });
    }

    onDayClicked(day_idx){
        this.setState({ selectedDate: day_idx});
        //Test it @ React inspector: salectedDate is null, except when any of tha dates day is clicked! ;-)
    }

    render() {
        const
            { weatherData, dates, city, selectedDate } = this.state
            //Get each id of each zip code, to do the selectors choices:
            , zips = weatherData.map( w=>w.id)

        ;

        return(
            <div className="app">

                <h1 className="text-center">Hi good people!</h1>
                <ZipForm
                    zips     = {zips}
                    onSubmit = { this.onFormSubmit }
                />

                <WeatherList
                    days         = {dates}
                    onDayClicked = {this.onDayClicked}
                />

                {selectedDate !== null && <CurrentDate day={dates[selectedDate]} city={city} />}
                
            </div>
        );
    }
}


export default App;