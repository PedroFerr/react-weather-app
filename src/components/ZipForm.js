import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ZipForm extends Component{

    //Initialize the satte of this component
    constructor(props){
        super(props);

        /* Since it's a select box, we don't ned to manage State anymore...
        //We access/set any property of the state of (this class of) the component through:
        this.state = {
          zipcode: ''
        };
        //From now on, "zipcode" is one State of this 'ZipForm' component - see it on React inspector at the browser, clicking on <ZipForm> object...

        //Do the binding of the input text:
        this.inputUpdated = this.inputUpdated.bind(this);
        //Having the same position above @ React inspector, if you type in, you'll see the updatings at the inspector!
        /**/

        //And the submit binding:
        this.submitZipCode = this.submitZipCode.bind(this);        

    }

    //Class methods:
    /**
    inputUpdated(evt){
        const
            {value} = evt.target

        ;
        this.setState({zipcode: value});
    }
    /**/

    submitZipCode(evt){
        //Don't refresh the page, on submiting the form:
        //evt.preventDefault();

        const
            //Get our internal State:
            //{zipcode}    = this.state
            //... and the handler coming from App.js, this parent Component:
            {onSubmit} = this.props

        ;
        //Finally we able  the communication between the parent and the child Components:
        //onSubmit(zipcode);
        onSubmit(evt.target.value);
        //... triggering the onSubmit event declared @ the JSX (HTML) of the parent, App.js

        //So now, picking up the React inspector again, this time @ <app></app>, we can see the State "zipcode" empty,
        //We type in and it's still empty - check <ZipForm></ZipForm> and, as seen above, the value is there! - but, onSubmit, the App gets the value from the input of the child component's form!
        //We're good; everything's working!   
        
        //Empty zipCode box input:
        //this.setState({ zipcode: '' });
    }

    render(){
        return(

            <div className="zip-form">
                <form onSubmit={this.submitZipCode} >
                    <label htmlFor="zipcode">Zip Code</label>

                    <select onChange={this.submitZipCode}>
                        <option value="">Select a zip</option>
                        {this.props.zips.map(zip =>
                            <option
                                //Here we have the UNIQUE key for React not to get lost, over a collection:
                                key={zip}
                                value={zip}
                            >{zip}</option>
                        )}
                    </select>

                </form>
            </div>

        );

    }
}

ZipForm.propTypes = {
    //zips, that are, on our parent component, "App.js", zips = weatherData.map( w=>w.id), will now ensure a type:
    zips      : PropTypes.arrayOf(PropTypes.number).isRequired
    // Our submitZipCode() function:
    , onSubmit: PropTypes.func
}
//At parent JSX's ZipForm component, delete HTML5 "zips" prop or set "onSubmit" tag to "3", for instances => you'll see the error at browser console!

export default ZipForm;