# React Weather App

This is my first tries on React library.

The challenge came from Beginner's React course @ [Scotch School](https://school.scotch.io/).

## Setup

Clone the project, install the dependencies, and run the project.

```
git clone https://github.com/searsaw/react-weather-app.git
cd react-weather-app
yarn
yarn start
```

Then open up your browser to http://localhost:8080, and you will be able to see the project.
